package com.github.loops;

public class Loops5 {
    public static void main(String[] args) {
        System.out.println("Посчитать сумму цифр заданного числа");
        System.out.println(sumNumbers(19));
    }

    public static int sumNumbers(int number) {
        int result = 0;
        while (number != 0){
            result += number % 10;
        number /= 10;
        }
        return result;
    }
}
