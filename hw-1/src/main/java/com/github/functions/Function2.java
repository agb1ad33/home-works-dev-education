package com.github.functions;

public class Function2 {

    public static void main(String[] args) {
        captcha(123);
    }


    static String captcha(int num) {

        int[] x = new int[4]; // массив разрядов
        int div = 1000; // делитель на разряды
        int var = num; // частное от деления
        int rest; // остаток от деления
        for (int i = 0; i < 4; i++) {
            rest = (var / div);
            x[i] = rest;
            var = var - rest * div;
            div = div / 10;
        }
        String result = "";
        String resultOne = "";
        String resultTwo = "";
        String resultTree = "";
        String resultFor = "";
        // Тысячи
        switch (x[0]) {
            case 1:
                resultOne = "Одна тысяча ";
                break;
            case 2:
                resultOne = "Две тысячи ";
                break;
            case 3:
                resultOne = "Три тысячи ";
                break;
            case 4:
                resultOne = "Четыре тысячи ";
                break;
            case 5:
                resultOne = "Пять тысяч ";
                break;
            case 6:
                resultOne = "Шесть тысяч ";
                break;
            case 7:
                resultOne = "Семь тысяч ";
                break;
            case 8:
                resultOne = "Восемь тысяч ";
                break;
            case 9:
                resultOne = "Девять тысяч ";
                break;
            default:
                break;
        }
        result += resultOne;
        // Сотни
        switch (x[1]) {
            case 1:
                resultTwo = "сто ";
                break;
            case 2:
                resultTwo = "двести ";
                break;
            case 3:
                resultTwo = "триста ";
                break;
            case 4:
                resultTwo = "четыреста ";
                break;
            case 5:
                resultTwo = "пятьсот ";
                break;
            case 6:
                resultTwo = "шестьсот ";
                break;
            case 7:
                resultTwo = "семьсот ";
                break;
            case 8:
                resultTwo = "восемьсот ";
                break;
            case 9:
                resultTwo = "девятьсот ";
                break;
            default:
                break;
        }
        result += resultTwo;
        // Десятки
        switch (x[2]) {
            case 1:
                switch (x[3]) {
                    case 0:
                        resultTree = "десять ";
                        break;
                    case 1:
                        resultTree = "одиннадцать ";
                        break;
                    case 2:
                        resultTree = "двенадцать ";
                        break;
                    case 3:
                        resultTree = "тринадцать ";
                        break;
                    case 4:
                        resultTree = "четырнадцать ";
                        break;
                    case 5:
                        resultTree = "пятнадцать ";
                        break;
                    case 6:
                        resultTree = "шестнадцать ";
                        break;
                    case 7:
                        resultTree = "семнадцать ";
                        break;
                    case 8:
                        resultTree = "восемнадцать ";
                        break;
                    case 9:
                        resultTree = "девятнадцать ";
                        break;
                }
                x[3] = 0;
                break;
            case 2:
                resultTree = "двадцать ";
                break;
            case 3:
                resultTree = "тридцать ";
                break;
            case 4:
                resultTree = "сорок ";
                break;
            case 5:
                resultTree = "пятьдесят ";
                break;
            case 6:
                resultTree = "шестьдесят ";
                break;
            case 7:
                resultTree = "семьдесят ";
                break;
            case 8:
                resultTree = "восемьдесят ";
                break;
            case 9:
                resultTree = "девяносто ";
                break;
            default:
                break;
        }
        result += resultTree;
        // Единицы
        switch (x[3]) {
            case 1:
                resultFor = "один ";
                break;
            case 2:
                resultFor = "два ";
                break;
            case 3:
                resultFor = "три ";
                break;
            case 4:
                resultFor = "четыре ";
                break;
            case 5:
                resultFor = "пять ";
                break;
            case 6:
                resultFor = "шесть ";
                break;
            case 7:
                resultFor = "семь ";
                break;
            case 8:
                resultFor = "восемь ";
                break;
            case 9:
                resultFor = "девять ";
                break;
            default:
                break;
        }
        result+=resultFor;
        return result;
    }}