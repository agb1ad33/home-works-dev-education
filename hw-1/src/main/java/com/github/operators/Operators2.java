package com.github.operators;

public class Operators2 {
    public static void main(String[] args) {
        System.out.println("определить какой четверти принадлежит точка с координатами (х,у)");
        System.out.println(quarter(2,3));
    }
    public static int quarter(int a, int b) {
        int result;
        if (a > 0 && b > 0)
            result = 1;
        else if (a < 0 && b > 0)
            result = 2;
        else if (a < 0 && b < 0)
            result = 3;
        else result = 4;
        return result;
    }
}
