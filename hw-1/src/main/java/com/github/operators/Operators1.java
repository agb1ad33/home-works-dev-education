package com.github.operators;

public class Operators1 {
    public static void main(String[] args) {
        System.out.println("Если а – четное посчитать а*б, иначе а+б");
        System.out.println(oneL(2,3));
    }
    public static int oneL(int a, int b) {
        int result;
        if (a % 2 == 0)
            result = a * b;
        else
            result = a + b;
        return result;
    }
}