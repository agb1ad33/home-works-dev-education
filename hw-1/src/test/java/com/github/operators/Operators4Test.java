package com.github.operators;

import org.junit.Test;

import static org.junit.Assert.*;

public class Operators4Test {

    @Test
    public void max() {
        int max=Operators4.max(2,3,5);
        assertEquals(max, 33);
    }
}