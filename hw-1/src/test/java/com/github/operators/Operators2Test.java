package com.github.operators;

import org.junit.Test;

import static org.junit.Assert.*;

public class Operators2Test {

    @Test
    public void quarter() {
        int point=Operators2.quarter(2,3);
        assertEquals(point,1);

    }
}