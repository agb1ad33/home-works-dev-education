package com.github.operators;

import org.junit.Test;

import static org.junit.Assert.*;

public class Operators3Test {

    @Test
    public void positive() {
        int sum=Operators3.positive(1,2,3);
        assertEquals(sum, 6);
    }
}