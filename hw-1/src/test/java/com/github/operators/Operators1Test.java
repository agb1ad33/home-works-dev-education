package com.github.operators;

import org.junit.Test;

import static org.junit.Assert.*;

public class Operators1Test {

    @Test
    public void oneL() {
        int sum = Operators1.oneL(4, 5);
        assertEquals(sum, 20);
    }
}