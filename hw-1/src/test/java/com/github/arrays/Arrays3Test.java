package com.github.arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class Arrays3Test {

    @Test
    public void minIndex() {
        int minIndex = Arrays3.minIndex(new int[]{1, 2, 3});
        assertNotNull(minIndex);
        assertEquals(minIndex, 0);
    }
}