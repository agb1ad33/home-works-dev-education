package com.github.arrays;

import org.junit.Test;
import static org.junit.Assert.*;

public class Arrays6Test {

    @Test
    public void reversArr() {
        int[] reversArr = Arrays6.reversArr(new int[]{1, 2, 3});

         assertArrayEquals(reversArr, new int[]{3, 2, 1});

        //assertTrue(Arrays.equals(reversArr, new int[]{3, 2, 1})); 2 вариант

    }
}