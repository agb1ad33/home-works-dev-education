package com.github.arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class Arrays4Test {

    @Test
    public void maxIndex() {
        int maxIndex = Arrays4.maxIndex(new int[]{1, 2, 3});

        assertNotNull(maxIndex);
        assertEquals(maxIndex, 2);

    }
}