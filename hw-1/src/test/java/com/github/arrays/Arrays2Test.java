package com.github.arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class Arrays2Test {

    @Test
    public void maxElement() {
        int maxElement = Arrays2.maxElement(new int[]{1, 2, 3});

        assertNotNull(maxElement);
        assertEquals(maxElement,3);


    }
}