package com.github.arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class Arrays8Test {

    @Test
    public void revers() {
        int[] revers = Arrays8.revers(new int[]{1, 2, 3, 4, 5});
        assertNotNull(revers);
        assertEquals(revers, new int[]{1, 2, 3, 4, 5});
    }
}