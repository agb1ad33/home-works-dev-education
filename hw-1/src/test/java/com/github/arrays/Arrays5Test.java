package com.github.arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class Arrays5Test {

    @Test
    public void sumElementsIndex() {
        int sumElementsIndex = Arrays5.sumElementsIndex(new int[]{1,2,3,4,5});
        assertNotNull(sumElementsIndex);
        assertEquals(sumElementsIndex,9);
    }
}