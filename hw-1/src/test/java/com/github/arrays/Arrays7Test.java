package com.github.arrays;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotNull;

public class Arrays7Test {

    @Test
    public void sumNecElements() {
        int sum=Arrays7.sumNecElements(new int[]{1,2,3,4,5});
        assertNotNull(sum);
        assertEquals(sum,3);
    }
}