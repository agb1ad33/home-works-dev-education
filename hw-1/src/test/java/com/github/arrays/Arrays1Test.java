package com.github.arrays;

import org.junit.Test;

import static org.junit.Assert.*;

public class Arrays1Test {

    @Test
    public void minElement() {
        int minElement = Arrays1.minElement(new int[]{1, 2, 3, 4});

        assertNotNull(minElement);
        assertEquals(minElement,1);
    }
}