package com.github.loops;

import org.junit.Test;

import static org.junit.Assert.*;

public class Loops3Test {

    @Test
    public void sqrtOne() {
    int one=Loops3.sqrtOne(14);
    assertEquals(one,3);

    }

    @Test
    public void sqrtTwo() {
        int two = Loops3.sqrtTwo(14);
        assertEquals(two, 4);


    }}