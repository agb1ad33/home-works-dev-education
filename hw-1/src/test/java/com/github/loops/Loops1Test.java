package com.github.loops;

import org.junit.Test;

import static org.junit.Assert.*;

public class Loops1Test {

    @Test
    public void simpleNumber() {

        boolean simpleNumberTrue = Loops1.simpleNumber(1);   // проверяет труу
        boolean simpleNumberFalse = Loops1.simpleNumber(4);    // проверяет фолс
        assertTrue(simpleNumberTrue);
        assertFalse(simpleNumberFalse);
    }
}