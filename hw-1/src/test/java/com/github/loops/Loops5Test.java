package com.github.loops;

import org.junit.Test;

import static org.junit.Assert.*;

public class Loops5Test {

    @Test
    public void sumNumbers() {
        int sumNumbers = Loops5.sumNumbers(19);
        assertEquals(sumNumbers,10);
    }
}