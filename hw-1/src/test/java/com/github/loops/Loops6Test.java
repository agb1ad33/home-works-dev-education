package com.github.loops;

import org.junit.Test;

import static org.junit.Assert.*;

public class Loops6Test {

    @Test
    public void reversNumbers() {
        int num = Loops6.reversNumbers(34);
        assertEquals(num, 43);
    }
}