package com.github.loops;

import org.junit.Test;

import static org.junit.Assert.*;

public class Loops4Test {

    @Test
    public void factorial() {
        int num=Loops4.factorial(7);
        assertEquals(num,5040);
    }
}