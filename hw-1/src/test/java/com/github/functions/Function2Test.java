package com.github.functions;

import org.junit.Test;

import static org.junit.Assert.*;

public class Function2Test {

    @Test
    public void captcha() {
        String captcha = Function2.captcha(25);
        assertEquals(captcha, "двадцать пять");

    }
}