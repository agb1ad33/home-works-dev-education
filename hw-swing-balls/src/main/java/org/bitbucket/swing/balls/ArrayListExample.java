package org.bitbucket.swing.balls;

import java.util.ArrayList;

public class ArrayListExample {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        list.add("One");
        list.add("Two");
        list.add("Three");
        System.out.println(list);
        list.remove("Two");
        System.out.println(list);

    }
}
