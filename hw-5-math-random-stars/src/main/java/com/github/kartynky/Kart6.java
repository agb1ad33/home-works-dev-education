package com.github.kartynky;

public class Kart6 {
    public static void main(String[] args) {
        for (int i = 0; i < 7; i++) {
            for (int a = 0; a < 7; a++) {
                if (a == i || a + i == 6) {
                    System.out.print("*");
                } else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
}
