package com.github.kartynky;

public class Kart2 {
    public static void main(String[] args) {
        int a = 7;
        for (int i = 0; i < a; i++) {
            for (int q = 0; q < a; q++) {
                if (i == 0 || i == a - 1 || q == 0 || q == a - 1) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println(" ");
        }
    }
}
