package com.github.mathhh;

public class MathEx3 {
    public static void main(String[] args) {
        // p1 - p3: вершины треугольника, ptest: проверяемая точка.
// VEC - структура, содержащая поля X, Y, написанная нами.
// Можно вполне использовать POINT из <windows.h>
// Возвращается TRUE, если принадлежит, иначе - FALSE.

        int x = 1;
        int y = 1;


        int triangle1Dot1 = (2 - x) * (0 - y) - (1 - 2) * (2 - y);
        int triangle1Dot2 = (1 - x) * (0 - 0) - (0 - 1) * (0 - y);
        int triangle1Dot3 = (0) * (2 - 0) - (2 - 0) * (0 - y);

        int triangle2Dot1 = (1 - x) * (0 - 0) - (0 - 1) * (0 - y);
        int triangle2Dot2 = (0 - x) * (-1 - 0) - (0 - 0) * (0 - y);
        int triangle2Dot3 = (0 - x) * (0 - -1) - (1 - 0) * (-1 - y);

        int triangle3Dot1 = (-2 - x) * (0 - 0) - (0 - -2) * (2 - y);
        int triangle3Dot2 = (0 - x) * (0 - 0) - (-1 - 0) * (0 - y);
        int triangle3Dot3 = (-1 - x) * (2 - 0) - (-2 - (-1)) * (0 - y);

        int triangle4Dot1 = (-1 - x) * (0 - 0) - (0 - (-1)) * (0 - y);
        int triangle4Dot2 = (0 - x) * (-1 - 0) - (0 - 0) * (0 - y);
        int triangle4Dot3 = (0 - x) * (0 - (-1)) - (-1 - 0) * (-1 - y);

        if ((triangle1Dot1 >= 0 && triangle1Dot2 >= 0 && triangle1Dot3 >= 0) || (triangle1Dot1 <= 0 && triangle1Dot2 <= 0 && triangle1Dot3 <= 0) || (triangle2Dot1 >= 0 && triangle2Dot2 >= 0 && triangle2Dot3 >= 0) || (triangle2Dot1 <= 0 && triangle2Dot2 <= 0 && triangle2Dot3 <= 0) || (triangle3Dot1 >= 0 && triangle3Dot2 >= 0 && triangle3Dot3 >= 0) || (triangle3Dot1 <= 0 && triangle3Dot2 <= 0 && triangle3Dot3 <= 0) || (triangle4Dot1 >= 0 && triangle4Dot2 >= 0 && triangle4Dot3 >= 0) || (triangle4Dot1 <= 0 && triangle4Dot2 <= 0 && triangle4Dot3 <= 0))
            System.out.println(1);
        else
            System.out.println(0);
    }
}