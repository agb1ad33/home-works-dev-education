package org.bitbucket.log;

import org.apache.log4j.Logger;

public class RandomNumber {

    private int randomNumber;

    private static final Logger log = Logger.getLogger(String.valueOf(RandomNumber.class));

    public int getRandomNumber() {
        try {
            randomNumber = (int) (Math.random() * 10);
            if (randomNumber >= 5 ) {
                log.info("Приложение успешно запущено");
                return randomNumber;
            }
            throw new Exception("Сгенерированное число - " + randomNumber);
        } catch (Exception ex) {
            log.error(ex.getMessage());
            return randomNumber;
        }
    }
}
