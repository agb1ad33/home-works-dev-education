package org.bitbucket.tree;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class BSTreeTest {
    private final List list;
    private final String name;

    public BSTreeTest(String name, List list) {
        this.name = name;
        this.list = list;
    }

    public BSTreeTest(List list, String name) {
        this.list = list;
        this.name = name;
    }

    //    ****************************************************************
//    *************************initTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void init() {

    }
    //    *************************************************************
//    *************************printTest******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void print() {

    }
    //    ****************************************************************
//    *************************clearTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void clear() {
        int[] array = new int[]{1, 232, 43432, 123, 543, 4343, 123, 5644, 34, 12};
        this.List.init(array);
        this.List.clear();
        int[] exp = {};
        int[] act = this.List.toArray();
        assertArrayEquals(exp, act);
    }
    //    ****************************************************************
//    *************************sizeTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void size() {

    }
    //    ****************************************************************
//    *************************sizeNodeTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void sizeNode() {

    }
    //    ****************************************************************
//    *************************toArrayTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void toArray() {

    }
    //    ****************************************************************
//    *************************toArrayNodeTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void toArrayNode() {

    }
    //    ****************************************************************
//    *************************addTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void add() {

    }
    //    ****************************************************************
//    *************************delTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void del() {

    }
    //    ****************************************************************
//    *************************getWidthTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void getWidth() {

    }
    //    ****************************************************************
//    *************************getHeightTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void getHeight() {

    }
    //    ****************************************************************
//    *************************nodesTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void nodes() {

    }
    //    ****************************************************************
//    *************************leavesTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void leaves() {

    }
    //    ****************************************************************
//    *************************reverseTest*******************************
//    ****************************************************************
//    ****************************************************************
    @Test
    public void reverse() {

    }
}