package org.bitbucket.tree;

import java.util.Objects;

public class BSTree implements ITree {

    private Node root;

    private static class Node {

        int value;

        Node right;

        Node left;

        public Node(int value) {
            this.value = value;
        }
    }

    @Override
    public void init(int[] init) {
        if (Objects.isNull(init)) {
            init = new int[0];
        }
        for (int elem : init) {
            add(elem);
        }
    }

    @Override
    public void print() {
    }

    @Override
    public void clear() {
        this.root = null;
    }

    @Override
    public int size() {
        return sizeNode(this.root);
    }

    public int sizeNode(Node p) {
        if (p == null) {
            return 0;
        }
        int count = 0;
        count += sizeNode(p.right);
        count++;
        count += sizeNode(p.right);
        return count;
    }

    @Override
    public int[] toArray() {
        if(Objects.isNull(this.root)){
            return new int[0];
        }
        int[] array = new int[size()];
        Counter c = new Counter();
        toArrayNode(array, c, this.root);
        return array;
    }

    public void toArrayNode(int[]array, Counter c, Node node){
        if(Objects.isNull(node)){
            return;
        }
        toArrayNode(array, c, node.left);
        array[c.index++] = node.value;
        toArrayNode(array, c, node.right);
    }

    @Override
    public void add(int val) {
        if (Objects.isNull(this.root)) {
            this.root = new Node(val);
        }
        addNode(val, this.root);
    }

    private void addNode(int val, Node p) {
        if (val < p.value) {
            if (Objects.isNull(p.left)) {
                p.left = new Node(val);
            } else {
                addNode(val, p.left);
            }
        } else {
            if (Objects.isNull(p.right)) {
                p.right = new Node(val);
            } else {
                addNode(val, p.right);
            }
        }
    }

    @Override
    public void del(int val) {

    }

    @Override
    public int getWidth() {
        return 0;
    }

    @Override
    public int getHeight() {
        return 0;
    }

    @Override
    public int nodes() {
        return 0;
    }

    @Override
    public int leaves() {
        return 0;
    }

    @Override
    public void reverse() {

    }

    private static class Counter {
        int index = 0;
    }

}
