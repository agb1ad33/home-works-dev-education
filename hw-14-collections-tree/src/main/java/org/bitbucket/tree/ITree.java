package org.bitbucket.tree;

public interface ITree {

    void init(int[] ar);

    void print();

    void clear();

    int size();

    int[] toArray();

    void add(int val);

    void del(int val);

    int getWidth();

    int getHeight();

    int nodes();

    int leaves();

    void reverse();

}
