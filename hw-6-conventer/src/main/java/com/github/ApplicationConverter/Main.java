package com.github.ApplicationConverter;

import javax.swing.*;
import java.awt.*;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setLayout(new FlowLayout());
        frame.getContentPane().add(new ApplicationConverter.WeightConverter());
        frame.getContentPane().add(new TimeConverter());
        frame.getContentPane().add(new LengthConverter());
        frame.getContentPane().add(new VolumeConverter());
        frame.getContentPane().add(new TemperatureConverter());
        frame.setTitle("Convert Thing");
        frame.setSize(1000, 500);
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }


}
