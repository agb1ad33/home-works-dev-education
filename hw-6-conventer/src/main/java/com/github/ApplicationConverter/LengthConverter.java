package com.github.ApplicationConverter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class LengthConverter extends JPanel {
    enum Convert {
        m("meter"),
        km("kilometer"),
        mile ("mile"),
        n_mile("nautical mile"),
        cable("cable"),
        league("league"),
        foot("foot"),
        yard("yard");


        private String description;

        Convert(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.description;
        }
    }

    class ConvertPair {
        private final LengthConverter.Convert from;
        private final LengthConverter.Convert to;

        public ConvertPair(LengthConverter.Convert from, LengthConverter.Convert to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            LengthConverter.ConvertPair that = (LengthConverter.ConvertPair) o;
            if (from != that.from) return false;
            return to == that.to;
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }

    private final Map<LengthConverter.ConvertPair, BigDecimal> exchangeRates = new HashMap<LengthConverter.ConvertPair, BigDecimal>() {
        {
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.m), BigDecimal.valueOf(1));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.km), BigDecimal.valueOf(1));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.mile), BigDecimal.valueOf(1));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.n_mile), BigDecimal.valueOf(1));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.cable), BigDecimal.valueOf(1));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.league), BigDecimal.valueOf(1));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.foot), BigDecimal.valueOf(1));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.yard), BigDecimal.valueOf(1));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.km), BigDecimal.valueOf(0.001));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.mile), BigDecimal.valueOf(0.000621371));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.n_mile), BigDecimal.valueOf(0.000539957));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.cable), BigDecimal.valueOf(0.0054));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.league), BigDecimal.valueOf(0.000179986));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.foot), BigDecimal.valueOf(3.28084));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.m, LengthConverter.Convert.yard), BigDecimal.valueOf(1.09361));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.m), BigDecimal.valueOf(1000.0));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.mile), BigDecimal.valueOf(0.621371));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.n_mile), BigDecimal.valueOf(0.539957));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.cable), BigDecimal.valueOf(5.3961));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.league), BigDecimal.valueOf(0.179986));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.foot), BigDecimal.valueOf(3280.84));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.km, LengthConverter.Convert.yard), BigDecimal.valueOf(1093.61));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.m), BigDecimal.valueOf(1609.34));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.km), BigDecimal.valueOf(1.60934));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.n_mile), BigDecimal.valueOf(0.868976));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.cable), BigDecimal.valueOf(8.6842));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.league), BigDecimal.valueOf(0.289659));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.foot), BigDecimal.valueOf(5280));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.mile, LengthConverter.Convert.yard), BigDecimal.valueOf(1760));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.m), BigDecimal.valueOf(1852));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.km), BigDecimal.valueOf(1.852));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.mile), BigDecimal.valueOf(1.15078));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.cable), BigDecimal.valueOf(9.99));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.league), BigDecimal.valueOf(0.333333));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.foot), BigDecimal.valueOf(6076.12));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.n_mile, LengthConverter.Convert.yard), BigDecimal.valueOf(2025.37));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.m), BigDecimal.valueOf(185.2));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.km), BigDecimal.valueOf(0.1853));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.mile), BigDecimal.valueOf(0.115151515));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.n_mile), BigDecimal.valueOf(0.1001));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.league), BigDecimal.valueOf(0.038384092));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.foot), BigDecimal.valueOf(608));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.cable, LengthConverter.Convert.yard), BigDecimal.valueOf(202.66));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.m), BigDecimal.valueOf(5556));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.km), BigDecimal.valueOf(5.556));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.mile), BigDecimal.valueOf(3.45234));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.n_mile), BigDecimal.valueOf(3.0));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.cable), BigDecimal.valueOf(29.980));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.foot), BigDecimal.valueOf(18228.3));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.league, LengthConverter.Convert.yard), BigDecimal.valueOf(6076.12));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.m), BigDecimal.valueOf(0.3048));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.km), BigDecimal.valueOf(0.0003048));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.mile), BigDecimal.valueOf(0.000189394));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.n_mile), BigDecimal.valueOf(0.000164579));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.cable), BigDecimal.valueOf(0.0016));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.league), BigDecimal.valueOf(6.31313E-5 ));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.foot, LengthConverter.Convert.yard), BigDecimal.valueOf(0.333333));

            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.m), BigDecimal.valueOf(0.9144));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.km), BigDecimal.valueOf(0.0009144));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.mile), BigDecimal.valueOf(0.000568182));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.n_mile), BigDecimal.valueOf(0.000493737));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.cable), BigDecimal.valueOf(0.004934210));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.league), BigDecimal.valueOf(0.000164579));
            put(new LengthConverter.ConvertPair(LengthConverter.Convert.yard, LengthConverter.Convert.foot), BigDecimal.valueOf(3.0));

        }
    };

    public LengthConverter() {
        super(new FlowLayout(FlowLayout.LEADING));


        // From
        JPanel from = new JPanel();
        JComboBox fromOptions = new JComboBox(LengthConverter.Convert.values());
        from.add(fromOptions);
        from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
        add(from, BorderLayout.CENTER);

        // Amount
        JTextField amountInput = new JTextField(20);
        JPanel amount = new JPanel();
        amount.add(amountInput);
        amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
        add(amount, BorderLayout.CENTER);


        // To
        JComboBox toOptions = new JComboBox(LengthConverter.Convert.values());
        JPanel to = new JPanel();
        to.add(toOptions);
        to.setBorder(BorderFactory.createTitledBorder("Convert to"));
        add(to, BorderLayout.CENTER);

        // Convert Action
        JLabel convertText = new JLabel();
        JButton convertCmd = new JButton("Convert");
        convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
        JPanel convert = new JPanel();
        convert.add(convertCmd);
        convert.add(convertText);
        add(convert);
    }

    private ActionListener convertAction(
            final JTextField amountInput,
            final JComboBox fromOptions,
            final JComboBox toOptions,
            final JLabel convertText) {

        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String amountInputText = amountInput.getText();
                if ("".equals(amountInputText)) {
                    return;
                }

                // Convert
                BigDecimal conversion = convertConvert(amountInputText);
                convertText.setText(conversion.toString());
            }

            private BigDecimal convertConvert(String amountInputText) {
                LengthConverter.ConvertPair ConvertPair = new ConvertPair(
                        (Convert) fromOptions.getSelectedItem(),
                        (Convert) toOptions.getSelectedItem());
                BigDecimal rate = exchangeRates.get(ConvertPair);
                BigDecimal amount = new BigDecimal(amountInputText);
                return amount.multiply(rate);
            }
        };

    }
}