package com.github.ApplicationConverter;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class VolumeConverter extends JPanel {
    enum Convert {
        l("liter"),
        m3("m3"),
        gallon("gallon"),
        pint("pint"),
        quart("quart"),
        barrel("barrel"),
        cubic_foot("cubic_foot"),
        cubic_inch("cubic_inch");
        private String description;
        Convert(String description) {
            this.description = description;
        }

        @Override
        public String toString() {
            return this.name() + " - " + this.description;
        }
    }

    class ConvertPair {
        private final VolumeConverter.Convert from;
        private final VolumeConverter.Convert to;
        public ConvertPair(VolumeConverter.Convert from, VolumeConverter.Convert to) {
            this.from = from;
            this.to = to;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            VolumeConverter.ConvertPair that = (VolumeConverter.ConvertPair) o;
            if (from != that.from) return false;
            return to == that.to;
        }

        @Override
        public int hashCode() {
            int result = from.hashCode();
            result = 31 * result + to.hashCode();
            return result;
        }
    }
    private final Map<VolumeConverter.ConvertPair, BigDecimal> exchangeRates = new HashMap<VolumeConverter.ConvertPair, BigDecimal>() {
        {
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.l), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.m3), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.gallon), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.pint), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.quart), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.barrel), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(1));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.m3), BigDecimal.valueOf(0.001));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.gallon), BigDecimal.valueOf(0.219969248));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.pint), BigDecimal.valueOf(1.759753986));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.quart), BigDecimal.valueOf(0.879876993));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.barrel), BigDecimal.valueOf(0.006110256));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(0.03531466));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.l, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(61.0237));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.l), BigDecimal.valueOf(1000));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.gallon), BigDecimal.valueOf(219.9692482991));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.pint), BigDecimal.valueOf(1759.753986393));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.quart), BigDecimal.valueOf(879.8769931964));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.barrel), BigDecimal.valueOf(6.110256897197));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(35.31466672149));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.m3, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(61023.74409473));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.l), BigDecimal.valueOf(4.54609));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.m3), BigDecimal.valueOf(0.00454609));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.pint), BigDecimal.valueOf(8.0));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.quart), BigDecimal.valueOf(4.0));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.barrel), BigDecimal.valueOf(0.027777777));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(0.160543653));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.gallon, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(277.4194327916));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.l), BigDecimal.valueOf(0.56826125));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.m3), BigDecimal.valueOf(0.00056826125));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.gallon), BigDecimal.valueOf(0.125));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.quart), BigDecimal.valueOf(0.5));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.barrel), BigDecimal.valueOf(0.0034722));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(0.0200679));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.pint, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(34.6774290));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.l), BigDecimal.valueOf(1.1365225));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.m3), BigDecimal.valueOf(0.0011365225));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.gallon), BigDecimal.valueOf(0.25));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.pint), BigDecimal.valueOf(2.0));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.barrel), BigDecimal.valueOf(0.006944444));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(0.040135913));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.quart, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(69.3548581));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.l), BigDecimal.valueOf(163.65924));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.m3), BigDecimal.valueOf(0.16365924));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.gallon), BigDecimal.valueOf(36.0));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.pint), BigDecimal.valueOf(288.0));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.quart), BigDecimal.valueOf(144.0));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(5.779571516));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.barrel, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(9987.099580));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.l), BigDecimal.valueOf(28.3168465));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.m3), BigDecimal.valueOf(0.028316846));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.gallon), BigDecimal.valueOf(6.2288354));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.pint), BigDecimal.valueOf(49.8306836));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.quart), BigDecimal.valueOf(24.9153418));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.barrel), BigDecimal.valueOf(0.17302320));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_foot, VolumeConverter.Convert.cubic_inch), BigDecimal.valueOf(1728));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.l), BigDecimal.valueOf(0.016387));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.m3), BigDecimal.valueOf(0.0000163));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.gallon), BigDecimal.valueOf(0.00360465));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.pint), BigDecimal.valueOf(0.02883720));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.quart), BigDecimal.valueOf(0.0144186005));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.barrel), BigDecimal.valueOf(0.0001001291));
            put(new VolumeConverter.ConvertPair(VolumeConverter.Convert.cubic_inch, VolumeConverter.Convert.cubic_foot), BigDecimal.valueOf(0.000578703));
        }
    };
    public VolumeConverter() {
        super(new FlowLayout(FlowLayout.LEADING));
        JPanel from = new JPanel();
        JComboBox fromOptions = new JComboBox(VolumeConverter.Convert.values());
        from.add(fromOptions);
        from.setBorder(BorderFactory.createTitledBorder("Select Convert"));
        add(from, BorderLayout.CENTER);
        JTextField amountInput = new JTextField(20);
        JPanel amount = new JPanel();
        amount.add(amountInput);
        amount.setBorder(BorderFactory.createTitledBorder("Enter Ammount"));
        add(amount, BorderLayout.CENTER);
        JComboBox toOptions = new JComboBox(VolumeConverter.Convert.values());
        JPanel to = new JPanel();
        to.add(toOptions);
        to.setBorder(BorderFactory.createTitledBorder("Convert to"));
        add(to, BorderLayout.CENTER);Action
        JLabel convertText = new JLabel();
        JButton convertCmd = new JButton("Convert");
        convertCmd.addActionListener(convertAction(amountInput, fromOptions, toOptions, convertText));
        JPanel convert = new JPanel();
        convert.add(convertCmd);
        convert.add(convertText);
        add(convert);
    }
    private ActionListener convertAction(
            final JTextField amountInput,
            final JComboBox fromOptions,
            final JComboBox toOptions,
            final JLabel convertText) {
        return new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String amountInputText = amountInput.getText();
                if ("".equals(amountInputText)) {
                    return;
                }
                BigDecimal conversion = convertConvert(amountInputText);
                convertText.setText(conversion.toString());
            }
            private BigDecimal convertConvert(String amountInputText) {
                VolumeConverter.ConvertPair ConvertPair = new ConvertPair(
                        (Convert) fromOptions.getSelectedItem(),
                        (Convert) toOptions.getSelectedItem());
                BigDecimal rate = exchangeRates.get(ConvertPair);
                BigDecimal amount = new BigDecimal(amountInputText);
                return amount.multiply(rate);
            }
        };
    }
}
