package com.github.http_server;

public enum Type {
    PUT("PUT"),
    POST("POST"),
    GET("GET"),
    DELETE("DELETE");

    private final String value;

    Type(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.value;
    }

    public static Type getValue(String value) {
        for (Type v : values())
            if (v.getValue().equalsIgnoreCase(value)) return v;
        throw new IllegalArgumentException("Not allowed extension.");
    }
}
