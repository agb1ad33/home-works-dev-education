package com.github.http_server;

import java.io.IOException;
import java.net.Socket;

public class ConnectionThread implements Runnable {
    private final Socket socket;

    public ConnectionThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            Thread.sleep(5000);
            socket.close();
        } catch (IOException | InterruptedException exception) {
            exception.printStackTrace();
        }
    }
}
