package org.bitbucket.app;

public class ServerRun {
    public static void main(String[] args) {
        WebSocket webSocket = new WebSocket();
        Thread thread = new Thread(webSocket, "WebSocket");
        System.out.println(thread.getName() + " run");
        thread.start();
    }
}
