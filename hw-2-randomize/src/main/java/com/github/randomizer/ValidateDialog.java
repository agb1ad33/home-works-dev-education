package com.github.randomizer;

import java.util.InputMismatchException;

public class ValidateDialog {

    static int min;

    static int max;

    public static void validateDialog() {
        while (true) {
            try {
                System.out.println("Введите минимальное значение(>0)");
                min = ScannerWrapper.getNumber();
                if (1 > min) {
                    System.out.println("Минимальное число должно быть больше нуля");
                    continue;
                }
                System.out.println("Введите Максимальное значение(<500)");
                max = ScannerWrapper.getNumber();
                if (1 > max) {
                    System.out.println("Масимальное число должно быть больше нуля");
                    continue;
                }
                if (max > 500) {
                    System.out.println("Максимальное число больше 500.");
                    continue;
                }
                if (max < min) {
                    System.out.println("ПОДУМАЙ, И ПОПРОБУЙ ЕЩЕ РАЗ!!!!");
                    continue;
                }
                break;
            } catch (InputMismatchException e) {
                System.out.println("Водить надо число.. Альоу");
            }
        }
    }

}
