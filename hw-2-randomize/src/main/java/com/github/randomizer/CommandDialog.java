package com.github.randomizer;

import java.util.List;

public class CommandDialog {

    public static void commandsDialog(List<Integer> listNumbers, int max, int min) {
        switch (ScannerWrapper.getCommand()) {
            case "generate":
                generationNumber(listNumbers, max, min);
                break;
            case "exit":
                System.exit(0);
            case "help":
                System.out.println("НАПИШИ 1 ЧТОБЫ СГЕНЕРИРОВАТЬ ЧИСЛО. 2 ЧТОБЫ ВЫЙТИ. 3 ПОМОЩЬ");
                break;
            default:
                System.out.println("Ой. Что-то пошло не так :( Нажми 3 для помощи");
                break;
        }
    }

    private static void generationNumber(List<Integer> listNumbers, int max, int min) {
        int generationNumber;
        while (true) {
            generationNumber = MathRandom.generateRandomNumber(min, max);
            if (listNumbers.contains(generationNumber)) {
                continue;
            }
            if (listNumbers.size() == max - min + 1) {
                System.out.println("Чисел больше нет ");
                System.exit(0);
            }
            listNumbers.add(generationNumber);
            System.out.println("Your random number is " + generationNumber);
            System.out.println("==================================");
            break;
        }
    }

}
