package com.github.randomizer;

import java.util.ArrayList;
import java.util.List;

public class RandomizerDialog {

    public static void randomizer() {
        ValidateDialog.validateDialog();
        int max = ValidateDialog.max;
        int min = ValidateDialog.min;
        System.out.println("Минимальное число " + min);
        System.out.println("Максимальное число " + max);
        List<Integer> listNumbers = new ArrayList<>();
        while (true) {
            System.out.println("НАПИШИ 1 ЧТОБЫ СГЕНЕРИРОВАТЬ ЧИСЛО. 2 ЧТОБЫ ВЫЙТИ. 3 ПОМОЩЬ ");
            System.out.println("1)generate\n2)exit\n3)help");
            CommandDialog.commandsDialog(listNumbers, max, min);
        }
    }

}
