package com.github.randomizer;

public class MathRandom {

    public static int generateRandomNumber(int min, int max) {
        return (int) (min + Math.random() * max);
    }

}
