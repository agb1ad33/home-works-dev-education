package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(reverseString("12345"));
    }

    public static String reverseString(String str){
        StringBuffer stringBuffer = new StringBuffer(str);
        return stringBuffer.reverse().toString();
    }
}
