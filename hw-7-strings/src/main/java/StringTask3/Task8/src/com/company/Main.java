package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(deleteLastWord("2312 123 4"));
    }


    public static String deleteLastWord(String x) {
        String[] arr = x.split(" ");
        arr[arr.length - 1] = "";
        StringBuffer lastResult = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            lastResult.append(arr[i] + " ");
        }
        return lastResult.toString();

    }
}
