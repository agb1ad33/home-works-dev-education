package com.company;

public class Task3 {

    public static void main(String[] args) {
        System.out.println(addSpace("1 3, 343,2"));
    }

    public static String addSpace(String x) {

        return x.replace(",", ", ").replace("  ", " ");
    }
}
