package test;


import com.company.Task3;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParamTest {
    private String valueA;
    private String expected;

    public ParamTest(String valueA,  String expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"1 3, 343,2" ,"1 3, 343, 2"},
                { "1 3, 33,2" ,"1 3, 33, 2"},

        });
    }

    @Test
    public void stringToIntTest() {
        assertEquals(expected,  new Task3().addSpace(valueA));
    }


}
