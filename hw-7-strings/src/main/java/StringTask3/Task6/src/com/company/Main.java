package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(delete("1232123",3,5));
    }

    public static String delete(String ar, int from, int to) {

        char[] arr = ar.toCharArray();
        for (int i = from; i < to; i++) {
            arr[i] = ' ';
        }
        String q = String.copyValueOf(arr);

        return q.replace(" ", "");
    }
}
