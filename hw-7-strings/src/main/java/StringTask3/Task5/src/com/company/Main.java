package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(countWords("ку івів вівіфффф"));
    }

    public static int countWords(String str){
        int count = 0;
        if(str.length() != 0){
            count++;
            for (int i = 0; i < str.length(); i++) {
                if(str.charAt(i) == ' '){
                    count++;
                }
            }
        }
        return count;
    }
}
