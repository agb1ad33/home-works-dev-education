package com.company;

public class Task1 {

    public static void main(String[] args) {
        System.out.println(lengthOfWordInString("12 34 3 13"));
    }

    public static int lengthOfWordInString(String ar) {

        String results = ar.replace(",", "");
        String[] arr = results.split(" ");

        int count = arr[0].length();

        for (int i = 0; i < arr.length; i++) {

            if (count > arr[i].length()) {

                count = arr[i].length();
            }
        }
        return count;
    }


}
