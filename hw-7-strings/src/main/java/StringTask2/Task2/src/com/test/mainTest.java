package com.test;

import com.company.Main;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testDoubleToStringTest() {
        String actual = "5.2";

        String expected = Main.doubleToString(5.2);

        Assert.assertEquals(actual, expected);

    }
}
