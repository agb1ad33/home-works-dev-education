package com.test;

import com.company.Main;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testIntToStringTest() {
        String actual = "5";

        String expected = Main.intToString(5);

        Assert.assertEquals(actual, expected);

    }
}
