package test;

import com.company.Main;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


import java.util.Arrays;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class ParamTest {
    private String valueA;
    private int expected;

    public ParamTest(String valueA, Integer expected) {
        this.valueA = valueA;
        this.expected = expected;
    }

    @Parameterized.Parameters()
    public static Iterable<Object[]> Test() {
        return Arrays.asList(new Object[][]{
                {"5", 5},
                {"6", 6}

        });
    }

    @Test
    public void stringToIntTest() {
        assertEquals(expected,  new Main().stringToInt(valueA));
    }


}
