package com.test;

import com.company.Task4;
import org.junit.Assert;
import org.junit.Test;

public class mainTest {
    @Test
    public void testIntToStringTest() {
        double actual = 5.2;

        double expected = Task4.stringToDouble("5.2");

        Assert.assertEquals(actual, expected,0.1);

    }
}
