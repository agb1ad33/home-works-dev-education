package com.github.ugadaika;

import java.util.Scanner;

public class Ugadai {
    static final int MIN = 1;
    static final int MAX = 200;
    static final int ATTEMPTS = 5;

    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int generationNumber = (int) (MIN + Math.random() * MAX);
        System.out.println("Привет, я загадал число от " + MIN + " до " + MAX + " вашего диапазона." +
                " Попробуй угадать его за " + ATTEMPTS + " попыток!");

        String str;
        int number;
        int past = 0;
        int attempt = 0;
        boolean firstAttempt = true;
        while (true) {
            if (attempt != ATTEMPTS) {
                try {
                    str = scanner.nextLine();
                    if (str.equals("exit")){
                        System.out.println("Выход с програмы!");
                        System.exit(0);
                    }
                    number = Integer.parseInt(str);
                } catch (Exception e) {
                    System.out.println("Ээээ не. Либо число либо exit. Ok??!");
                    continue;
                }
                if (firstAttempt) {
                    if (number == generationNumber) {
                        System.out.println("А чё, как так то?! Ты кто такой? С первого раза угадывать это не нормально вроде :/");
                        break;
                    }
                    firstAttempt = false;
                    System.out.println("Не угадал!");
                    attempt++;
                    past = number;
                    continue;
                }
                if (generationNumber == number) {
                    System.out.println("Молодец ты угадал! С " + attempt + " попытки!");
                } else if (Math.abs(generationNumber) - past > Math.abs(generationNumber - number)) {
                    System.out.println("Теплее");
                } else {
                    System.out.println("Холоднее");
                }
                past = number;
                attempt++;
            } else {
                System.out.println("Закончились попытки!");
                System.exit(1);
            }
        }
    }
}
