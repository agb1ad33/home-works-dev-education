package sss;

import org.bitbucket.streams.StreamStudents;
import org.bitbucket.streams.Student;
import org.junit.Test;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;

public class StreamTest {

    List<Student> studentList;
    List<Student> filteredList;


    //******** Collect for each faculty and course **********

    @Test
    public void listOfStudentsFacultyCourseOne() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getSecondExample(), Mock.getThirdExample());
        this.filteredList = StreamStudents.collectFacultyAndCourse(this.studentList, "TestFaculty1", "TestCourse1");
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void listOfStudentsFacultyCourseTwo() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample());
        this.filteredList = StreamStudents.collectFacultyAndCourse(this.studentList, "TestFaculty", "TestCourse");
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void listOfStudentsFacultyCourseThree() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFifthExample());
        this.filteredList = StreamStudents.collectFacultyAndCourse(this.studentList, "TestFaculty2", "TestCourse2");
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsFacultyCourseNoMatch() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFourthExample());
        this.filteredList = StreamStudents.collectFacultyAndCourse(this.studentList, "TestFaculty3", "TestCourse3");
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsFacultyCourseZeroList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        this.filteredList = StreamStudents.collectFacultyAndCourse(this.studentList, "TestFaculty3", "TestCourse3");
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsFacultyCourseNullList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        this.filteredList = StreamStudents.collectFacultyAndCourse(this.studentList, "TestFaculty3", "TestCourse3");
    }


    //******** Collect students by age **********************

    @Test
    public void listOfStudentsByAgeOne() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample(), Mock.getThirdExample());
        this.filteredList = StreamStudents.collectByAge(this.studentList, 1998);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void listOfStudentsByAgeTwo() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample(), Mock.getSecondExample(), Mock.getThirdExample(), Mock.getFourthExample());
        this.filteredList = StreamStudents.collectByAge(this.studentList, 1995);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void listOfStudentsByAgeThree() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample(), Mock.getSecondExample(), Mock.getThirdExample(), Mock.getFourthExample());
        this.filteredList = StreamStudents.collectByAge(this.studentList, 1995);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsByAgeException() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        this.filteredList = StreamStudents.collectByAge(this.studentList, 2005);
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsByAgeNullList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsNull);
        this.filteredList = StreamStudents.collectByAge(this.studentList, 2001);
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsByAgeZeroList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        this.filteredList = StreamStudents.collectByAge(this.studentList, 1995);
    }

    @Test
    public void listOfStudentsFacultyFirst() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getSecondExample(), Mock.getThirdExample());
        this.filteredList = StreamStudents.returnByFaculty(this.studentList, "TestFaculty1");
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void listOfStudentsFacultyDefault() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample(), Mock.getFourthExample());
        this.filteredList = StreamStudents.returnByFaculty(this.studentList, "TestFaculty");
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void listOfStudentsSecond() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFifthExample());
        this.filteredList = StreamStudents.returnByFaculty(this.studentList, "TestFaculty2");
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsException() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        this.filteredList = StreamStudents.returnByFaculty(this.studentList, "TestFaculty45");
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsNullList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsNull);
        this.filteredList = StreamStudents.returnByFaculty(this.studentList, "TestFaculty45");
    }

    @Test(expected = NullPointerException.class)
    public void listOfStudentsZeroList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        this.filteredList = StreamStudents.returnByFaculty(this.studentList, "TestFaculty45");
    }
    //*********** Collect first match by age ****************

    @Test
    public void findFirstMatchAgeFirst() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample());
        this.filteredList = StreamStudents.findFirstByAge(this.studentList, 1996);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void findFirstMatchAgeSecond() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample());
        this.filteredList = StreamStudents.findFirstByAge(this.studentList, 1997);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void findFirstMatchAgeThird() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample());
        this.filteredList = StreamStudents.findFirstByAge(this.studentList, 1994);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test
    public void findFirstMatchAgeFourth() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getFirstExample());
        this.filteredList = StreamStudents.findFirstByAge(this.studentList, 1998);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void findFirstMatchAgeNoMatch() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<Student> exp = Arrays.asList(Mock.getThirdExample());
        this.filteredList = StreamStudents.findFirstByAge(this.studentList, 2005);
        List<Student> act = this.filteredList;
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void findFirstMatchAgeNullList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsNull);
        this.filteredList = StreamStudents.findFirstByAge(this.studentList, 1995);
    }

    @Test(expected = NullPointerException.class)
    public void findFirstMatchAgeZeroList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        this.filteredList = StreamStudents.findFirstByAge(this.studentList, 1995);
    }

    //*******************************************************
    //********************* Count test **********************
    //*******************************************************

    @Test
    public void countByFaculty() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        long exp = 2;
        long act = StreamStudents.countByFaculty(this.studentList, "TestFaculty1");
        assertEquals(exp, act);
    }

    @Test
    public void countByFacultySecond() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        long exp = 2;
        long act = StreamStudents.countByFaculty(this.studentList, "TestFaculty");
        assertEquals(exp, act);
    }

    @Test
    public void countByFacultyThird() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        long exp = 1;
        long act = StreamStudents.countByFaculty(this.studentList, "TestFaculty2");
        assertEquals(exp, act);
    }

    @Test
    public void countByFacultyNoMatch() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        long exp = 0;
        long act = StreamStudents.countByFaculty(this.studentList, "TestFaculty3");
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void countByFacultyNullList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsNull);
        long exp = 0;
        long act = StreamStudents.countByFaculty(this.studentList, "TestFaculty3");
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void countByFacultyZeroList() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        long exp = 0;
        long act = StreamStudents.countByFaculty(this.studentList, "TestFaculty3");
        assertEquals(exp, act);
    }

    //*******************************************************
    //******************** Map test *************************
    //*******************************************************

    //**************** Set other faculty ********************

    @Test
    public void setFacultyFirst() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.getListOfStudentsOne());
        List<Student> exp = Arrays.asList(Mock.getListOfChangedFacultyOne());
        List<Student> act = StreamStudents.setNewFaculty(this.studentList, "TestFaculty", "NewFaculty");
        assertEquals(exp, act);
    }

    @Test
    public void setFacultyTwo() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.getFacultyToChangeOfTwo());
        List<Student> exp = Arrays.asList(Mock.getChangedFacultyTwo());
        List<Student> act = StreamStudents.setNewFaculty(this.studentList, "TestFaculty1", "NewFaculty");
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void setFacultyZeroList() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsZero());
        List<Student> exp = Arrays.asList(Mock.getChangedFacultyTwo());
        List<Student> act = StreamStudents.setNewFaculty(this.studentList, "TestFaculty1", "NewFaculty");
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void setFacultyNullList() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsNull());
        List<Student> exp = Arrays.asList(Mock.getChangedFacultyTwo());
        List<Student> act = StreamStudents.setNewFaculty(this.studentList, "TestFaculty1", "NewFaculty");
        assertEquals(exp, act);
    }
    //**** Collect new list in format - Name, Last Name *******

    @Test
    public void sortByNameLastNameFull() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        List<String> exp = new ArrayList<>(Arrays.asList("Test2, Test2", "Test3, Test3"));
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupTwo");
        assertEquals(exp, act);
    }

    @Test
    public void sortByNameLastNameFour() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFour);
        List<String> exp = new ArrayList<>(Arrays.asList("Test, Test", "Test1, Test1"));
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupOne");
        assertEquals(exp, act);
    }

    @Test
    public void sortByNameLastNameThree() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.listOfStudentsThree);
        List<String> exp = new ArrayList<>(Arrays.asList("Test, Test", "Test1, Test1"));
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupOne");
        assertEquals(exp, act);
    }

    @Test
    public void sortByNameLastNameTwo() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.listOfStudentsTwo);
        List<String> exp = new ArrayList<>(Arrays.asList("Test, Test", "Test1, Test1"));
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupOne");
        assertEquals(exp, act);
    }

    @Test
    public void sortByNameLastNameOne() {
        this.studentList = null;
        this.studentList = Arrays.asList(Mock.listOfStudentsOne);
        List<String> exp = new ArrayList<>(Arrays.asList("Test, Test"));
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupOne");
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sortByNameLastNameNoMatch() {
        this.studentList = Arrays.asList(Mock.listOfStudentsOne);
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupThree");
    }

    @Test(expected = NullPointerException.class)
    public void sortByNameLastNameZero() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        List<String> exp = new ArrayList<>();
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupOne");
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void sortByNameLastNameNull() {
        this.studentList = Arrays.asList(Mock.listOfStudentsNull);
        List<String> exp = new ArrayList<>();
        List<String> act = StreamStudents.sortByFnLn(this.studentList, "GroupOne");
        assertEquals(exp, act);
    }

    //*******************************************************
    //******************* Reduce test ***********************
    //*******************************************************

    @Test
    public void getStringOfStudentOne() {
        this.studentList = Arrays.asList(Mock.firstExample);
        String act = StreamStudents.studentsReduce(this.studentList);
        String exp = "Test, Test\nTestFaculty, GroupOne\n";
        assertEquals(exp, act);
    }

    @Test
    public void getStringOfStudentTwo() {
        this.studentList = Arrays.asList(Mock.listOfStudentsTwo);
        String act = StreamStudents.studentsReduce(this.studentList);
        String exp = "Test, Test\nTestFaculty, GroupOne\n" +
                "Test1, Test1\nTestFaculty1, GroupOne\n";
        assertEquals(exp, act);
    }

    @Test
    public void getStringOfStudentThree() {
        this.studentList = Arrays.asList(Mock.listOfStudentsThree);
        String act = StreamStudents.studentsReduce(this.studentList);
        String exp = "Test, Test\nTestFaculty, GroupOne\n" +
                "Test1, Test1\nTestFaculty1, GroupOne\n" +
                "Test2, Test2\nTestFaculty1, GroupTwo\n";
        assertEquals(exp, act);
    }

    @Test
    public void getStringOfStudentFour() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFour);
        String act = StreamStudents.studentsReduce(this.studentList);
        String exp = "Test, Test\nTestFaculty, GroupOne\n" +
                "Test1, Test1\nTestFaculty1, GroupOne\n" +
                "Test2, Test2\nTestFaculty1, GroupTwo\n" +
                "Test3, Test3\nTestFaculty, GroupTwo\n";
        assertEquals(exp, act);
    }

    @Test
    public void getStringOfStudentFive() {
        this.studentList = Arrays.asList(Mock.listOfStudentsFull);
        String act = StreamStudents.studentsReduce(this.studentList);
        String exp = "Test, Test\nTestFaculty, GroupOne\n" +
                "Test1, Test1\nTestFaculty1, GroupOne\n" +
                "Test2, Test2\nTestFaculty1, GroupTwo\n" +
                "Test3, Test3\nTestFaculty, GroupTwo\n" +
                "Test4, Test4\nTestFaculty2, GroupThree\n";
        assertEquals(exp, act);
    }

    @Test(expected = NullPointerException.class)
    public void getStringOfStudentsListNull() {
        this.studentList = Arrays.asList(Mock.listOfStudentsNull);
        String act = StreamStudents.studentsReduce(this.studentList);
    }

    @Test(expected = NullPointerException.class)
    public void getStringOfStudentsListZero() {
        this.studentList = Arrays.asList(Mock.listOfStudentsZero);
        String act = StreamStudents.studentsReduce(this.studentList);
    }

    //*************** All students on faculty ***************

    @Test
    public void allStudentsOnSameFaculty() {
        this.studentList = Arrays.asList(Mock.getSameFaculty());
        boolean act = StreamStudents.allStudentsOnFaculty(this.studentList, "TestFaculty1");
        assertTrue(act);
    }

    @Test
    public void allStudentsNotOnSameFaculty() {
        this.studentList = Arrays.asList(Mock.getSameFaculty());
        boolean act = StreamStudents.allStudentsOnFaculty(this.studentList, "TestFaculty2");
        assertFalse(act);
    }

    @Test(expected = NullPointerException.class)
    public void allStudentsOnFacultyZeroList() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsZero());
        boolean act = StreamStudents.allStudentsOnFaculty(this.studentList, "TestFaculty2");
    }

    @Test(expected = NullPointerException.class)
    public void allStudentsOnFacultyNullList() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsNull());
        boolean act = StreamStudents.allStudentsOnFaculty(this.studentList, "TestFaculty2");
    }

    //*************** One student at set faculty ************

    @Test
    public void atleastOneStudentOnFacultyFull() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsFull());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty1");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentOnFacultyFour() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsFour());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty1");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentOnFacultyThree() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsThree());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty1");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentOnFacultyTwo() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsTwo());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty1");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentOnFacultyOne() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsOne());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty");
        assertTrue(act);
    }

    @Test(expected = NullPointerException.class)
    public void atleastOneStudentOnFacultyZero() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsZero());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty");
        assertTrue(act);
    }

    @Test(expected = NullPointerException.class)
    public void atleastOneStudentOnFacultyNull() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsNull());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty");
        assertTrue(act);
    }

    @Test
    public void noStudentOnFaculty() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsOne());
        boolean act = StreamStudents.atleastOneOnFaculty(this.studentList, "TestFaculty2");
        assertFalse(act);
    }
    //*************** At least one student in group ************

    @Test
    public void atleastOneStudentInGroupFull() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsFull());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupOne");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentInGroupFour() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsFour());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupOne");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentInGroupThree() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsThree());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupOne");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentInGroupTwo() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsTwo());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupOne");
        assertTrue(act);
    }

    @Test
    public void atleastOneStudentInGroupOne() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsOne());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupOne");
        assertTrue(act);
    }

    @Test(expected = NullPointerException.class)
    public void atleastOneStudentInGroupZero() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsZero());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupOne");
        assertTrue(act);
    }

    @Test(expected = NullPointerException.class)
    public void atleastOneStudentInGroupNull() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsNull());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupOne");
        assertTrue(act);
    }

    @Test
    public void noStudentInGroup() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsOne());
        boolean act = StreamStudents.atleastOneOnGroup(this.studentList, "GroupThree");
        assertFalse(act);
    }

    //*************** All students on same group ***************

    @Test
    public void allStudentsInSameGroup() {
        this.studentList = Arrays.asList(Mock.getSameGroup());
        boolean act = StreamStudents.allStudentsOnGroup(this.studentList, "GroupTwo");
        assertTrue(act);
    }

    @Test
    public void allStudentsNotInSameGroup() {
        this.studentList = Arrays.asList(Mock.getSameGroup());
        boolean act = StreamStudents.allStudentsOnGroup(this.studentList, "GroupOne");
        assertFalse(act);
    }

    @Test(expected = NullPointerException.class)
    public void allStudentsInSameGroupZeroList() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsZero());
        boolean act = StreamStudents.allStudentsOnGroup(this.studentList, "GroupTwo");
    }

    @Test(expected = NullPointerException.class)
    public void allStudentsInSameGroupNullList() {
        this.studentList = Arrays.asList(Mock.getListOfStudentsNull());
        boolean act = StreamStudents.allStudentsOnGroup(this.studentList, "GroupTwo");
    }
}
