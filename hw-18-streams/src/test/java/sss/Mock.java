package sss;

import org.bitbucket.streams.Student;

public class Mock {

    public static final Student firstExample = new Student(1,
            "Dada",
            "Adad",
            2000,
            "SmthAdress",
            "+380123456789",
            "Faculty",
            "Course",
            "GroupOne");

    public static final Student secondExample = new Student(1,
            "Dada1",
            "Adad1",
            2001,
            "SmthAdress1",
            "+380123456780",
            "Faculty1",
            "Course1",
            "GroupOne");

    public static final Student thirdExample = new Student(1,
            "Dada2",
            "Adad2",
            2002,
            "SmthAdress2",
            "+380123456781",
            "Faculty1",
            "Course1",
            "GroupTwo");

    public static final Student fourthExample = new Student(1,
            "Dada3",
            "Adad3",
            2003,
            "TSmthAdress3",
            "+380123456782",
            "Faculty",
            "Course1",
            "GroupTwo");

    public static final Student fifthExample = new Student(1,
            "Dada4",
            "Adad4",
            2004,
            "SmthAdress4",
            "+380123456783",
            "Faculty2",
            "Course2",
            "GroupThree");

    public static final Student changedStudentGroupOne = new Student(1,
            "Dada",
            "Adad",
            2000,
            "SmthAdress",
            "+380123456789",
            "Faculty",
            "Course",
            "NewGroup");

    public static final Student changedStudentGroupTwo = new Student(1,
            "Dada",
            "Adad",
            2000,
            "SmthAdress",
            "+380123456789",
            "Faculty",
            "Course",
            "NewGroup");

    public static final Student changedFacultyOnlyOne = new Student(1,
            "Dada",
            "Adad",
            2000,
            "SmthAdress",
            "+380123456789",
            "Faculty",
            "Course",
            "GroupOne");

    public static final Student changedFacultyOne = new Student(1,
            "Dada1",
            "Adad",
            2001,
            "SmthAdress1",
            "+380123456780",
            "Faculty",
            "Course1",
            "GroupOne");

    public static final Student changedFacultyTwo = new Student(1,
            "Dada2",
            "Adad2",
            2002,
            "SmthAdress2",
            "+380123456781",
            "Faculty",
            "Course1",
            "GroupTwo");

    public static final Student[] listOfStudentsFull = {firstExample, secondExample, thirdExample, fourthExample, fifthExample};

    public static final Student[] listOfStudentsFour = {firstExample, secondExample, thirdExample, fourthExample};

    public static final Student[] listOfStudentsThree = {firstExample, secondExample, thirdExample};

    public static final Student[] listOfStudentsTwo = {firstExample, secondExample};

    public static final Student[] listOfStudentsOne = {firstExample};

    public static final Student[] listOfStudentsNull = null;

    public static final Student[] listOfStudentsZero = {};

    public static final Student[] sameFaculty = {secondExample, thirdExample};

    public static final Student[] sameGroup = {thirdExample, fourthExample};

    public static final Student[] facultyToChangeOfTwo = {secondExample, thirdExample};

    public static final Student[] listOfChangedFacultyOne = {changedFacultyOnlyOne};

    public static final Student[] listOfChangedFacultyTwo = {changedFacultyOne, changedFacultyTwo};

    public static final Student[] listOfChangedGroupOne = {changedStudentGroupOne};

    public static final Student[] listOfChangedGroupTwo = {changedStudentGroupOne, changedStudentGroupTwo};

    public static Student getFirstExample() {
        return firstExample;
    }

    public static Student getSecondExample() {
        return secondExample;
    }

    public static Student getThirdExample() {
        return thirdExample;
    }

    public static Student getFourthExample() {
        return fourthExample;
    }

    public static Student getFifthExample() {
        return fifthExample;
    }

    public static Student[] getListOfStudentsFull() {
        return listOfStudentsFull;
    }

    public static Student[] getListOfStudentsFour() {
        return listOfStudentsFour;
    }

    public static Student[] getListOfStudentsThree() {
        return listOfStudentsThree;
    }

    public static Student[] getListOfStudentsTwo() {
        return listOfStudentsTwo;
    }

    public static Student[] getListOfStudentsOne() {
        return listOfStudentsOne;
    }

    public static Student[] getListOfStudentsNull() {
        return listOfStudentsNull;
    }

    public static Student[] getListOfStudentsZero() {
        return listOfStudentsZero;
    }

    public static Student[] getSameFaculty() {
        return sameFaculty;
    }

    public static Student[] getSameGroup() {
        return sameGroup;
    }

    public static Student[] getListOfChangedFacultyOne() {
        return listOfChangedFacultyOne;
    }

    public static Student[] getChangedFacultyTwo() {
        return listOfChangedFacultyTwo;
    }

    public static Student[] getFacultyToChangeOfTwo() {
        return facultyToChangeOfTwo;
    }

    public static Student[] getListOfChangedGroupOne() {
        return listOfChangedGroupOne;
    }

    public static Student[] getListOfChangedGroupTwo() {
        return listOfChangedGroupTwo;
    }
}
