package com.github.streams;

import java.util.List;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.toList;

public class StreamStudents {

    public static List<Student> returnByFaculty(List<Student> originList, String faculty) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        List<Student> filteredList = originList
                .stream()
                .filter(c -> c.getFaculty().equals(faculty))
                .collect(toList());
        if (filteredList.isEmpty())
            throw new NullPointerException(".");
        return filteredList;
    }

    public static List<Student> collectFacultyAndCourse(List<Student> originList, String faculty, String course) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        List<Student> filteredList = originList
                .stream()
                .filter(c -> c.getFaculty().equals(faculty))
                .filter(c -> c.getCourse().equals(course))
                .collect(toList());
        if (filteredList.isEmpty())
            throw new NullPointerException(".");
        return filteredList;
    }

    public static List<Student> collectByAge(List<Student> originList, int age) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        List<Student> filteredList = originList.stream()
                .filter(c -> c.getYearOfBirth() >= age)
                .collect(toList());
        if (filteredList.isEmpty())
            throw new NullPointerException(".");
        return filteredList;
    }

    public static List<Student> findFirstByAge(List<Student> originList, int age) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        List<Student> filteredList = originList.stream()
                .filter(c -> c.getYearOfBirth() >= age)
                .findFirst().stream()
                .collect(toList());
        if (filteredList.isEmpty())
            throw new NullPointerException(".");
        return filteredList;
    }

    public static List<String> sortByFnLn(List<Student> originList, String group) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        List<String> newList = originList.stream()
                .filter(e -> e.getGroup().equals(group))
                .map(e -> e.getFirstName() + ", " + e.getLastName())
                .collect(Collectors.toList());
        if (newList.isEmpty())
            throw new NullPointerException(".");
        return newList;
    }

    public static long countByFaculty(List<Student> originList, String faculty) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        return originList.stream()
                .filter(c -> c.getFaculty().equals(faculty))
                .count();
    }

    public static List<Student> setNewFaculty(List<Student> originList, String faculty, String newFaculty) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        originList.stream()
                .filter(e -> e.getFaculty().equals(faculty))
                .map(e -> e.setFaculty(newFaculty))
                .collect(Collectors.toList());
        return originList;
    }

    public static List<Student> setNewGroup(List<Student> originList, String group, String newGroup) {
        if (originList.isEmpty())
            throw new NullPointerException("List is null");
        originList.stream()
                .filter(e -> e.getGroup().equals(group))
                .map(e -> e.setGroup(newGroup))
                .collect(Collectors.toList());
        return originList;
    }

    public static int studentsOnFaculty(List<Student> listOfStudents, String faculty) {
        int count;
        listOfStudents.stream()
                .filter(e -> e.getFaculty().equals(faculty))
                .findFirst();
        return 0;
    }

    public static String studentsReduce(List<Student> listOfStudents) {
        if (listOfStudents.isEmpty())
            throw new NullPointerException("List is null");
        return listOfStudents.stream()
                .map(e -> e.getFirstName() + ", " + e.getLastName() + "\n" + e.getFaculty() + ", " + e.getGroup() + "\n").reduce("", (a, b) -> a + b);
    }

    public static boolean allStudentsOnFaculty(List<Student> studentList, String faculty) {
        if (studentList.isEmpty())
            throw new NullPointerException("List is null");
        return studentList.stream().allMatch(e -> e.getFaculty().equals(faculty));
    }

    public static boolean atleastOneOnFaculty(List<Student> studentList, String faculty) {
        if (studentList.isEmpty())
            throw new NullPointerException("List is null");
        return studentList.stream().anyMatch(e -> e.getFaculty().equals(faculty));
    }

    public static boolean allStudentsOnGroup(List<Student> studentList, String group) {
        if (studentList.isEmpty())
            throw new NullPointerException("List is null");
        return studentList.stream().allMatch(e -> e.getGroup().equals(group));
    }

    public static boolean atleastOneOnGroup(List<Student> studentList, String group) {
        if (studentList.isEmpty())
            throw new NullPointerException("List is null");
        return studentList.stream().anyMatch(e -> e.getGroup().equals(group));
    }
}
