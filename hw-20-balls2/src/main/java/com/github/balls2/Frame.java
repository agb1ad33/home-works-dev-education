package com.github.balls2;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    public Panel panel = new Panel(this);

    public Frame() {
        setLayout(null);
        setSize(1000, 1000);
        panel.setBounds(10, 10, 740, 740);
        panel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
        add(panel);
        setVisible(Boolean.TRUE);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
