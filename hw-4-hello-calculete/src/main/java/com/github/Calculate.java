package com.github;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
public class Calculate {


    public static void main(String args[]) {

        JFrame mainFrame = new JFrame("calculator");
        JLabel jLabelOne = new JLabel("Число 1:");
        JLabel jLabelTwo = new JLabel("Число 2:");
        JLabel jLabelTree = new JLabel("Оператор:");
        JLabel jLabelFor = new JLabel("Результат:");

        JTextField jTextFieldFirstNumber = new JTextField(20);
        JTextField jTextFieldSecondNumber= new JTextField(20);
        JTextField jTextFieldOperator= new JTextField(20);
        JTextField jTextFieldResult= new JTextField(20);



        JButton jButtonResult = new JButton();
        Image img = null;
        try {
            img = ImageIO.read(new File("pac.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert img != null;
        jButtonResult.setPreferredSize(new Dimension(255,55));
        jButtonResult.setIcon(new ImageIcon(img));
        jButtonResult.addActionListener(e -> {
            String operator;
            Integer firstNumber;
            Integer secondNumber;
            try {
                operator = jTextFieldOperator.getText();
                firstNumber = Integer.valueOf(jTextFieldFirstNumber.getText());
                secondNumber = Integer.valueOf(jTextFieldSecondNumber.getText());
            }catch (Exception ex){
                jTextFieldResult.setText("Некоректный ввод");
                return;
            }
            Integer result;
            switch (operator) {
                case "+":
                    result = firstNumber + secondNumber;
                    break;
                case "-":
                    result = firstNumber - secondNumber;
                    break;
                case "*":
                    result = firstNumber * secondNumber;
                    break;
                case "/":
                    if (secondNumber == 0) {
                        jTextFieldResult.setText("На ноль не делим");
                        return;
                    } else
                        result = firstNumber / secondNumber;
                    break;
                default:
                    jTextFieldResult.setText("Некоретная операция");
                    return;
            }
            jTextFieldResult.setText(String.valueOf(result));
        });

        JPanel jPanel = new JPanel();
        jPanel.add(jLabelOne);
        jPanel.add(jTextFieldFirstNumber);
        jPanel.add(jLabelTwo);
        jPanel.add(jTextFieldSecondNumber);
        jPanel.add(jLabelTree);
        jPanel.add(jTextFieldOperator);
        jPanel.add(jButtonResult);
        jPanel.add(jLabelFor);
        jPanel.add(jTextFieldResult);
        jPanel.setBackground(Color.getHSBColor(100,200,100));

        mainFrame.add(jPanel);
        mainFrame.setSize(330, 230);
        mainFrame.show();
    }
}

