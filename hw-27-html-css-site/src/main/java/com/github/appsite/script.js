const sendContacts = document.getElementById('submitButton');
const buttonRight = document.getElementById('rightButton');
const sendGood = document.getElementById('contactsSendGood');
const buttonLeft = document.getElementById('leftButton');
const scrollContainer = document.getElementById('reviewMain');
buttonRight.onclick = function () {
    scrollContainer.scrollLeft += 800;
}
buttonLeft.onclick = function () {
    scrollContainer.scrollLeft -= 800;
}
sendContacts.addEventListener('click', (e) => {
    sendGood.style.transition = '0.5s';
    console.log('pressed');
    sendGood.style.opacity = '100';
    sendGood.style.visibility = 'visible';
    setTimeout(function () {
        console.log('got in timeout');
        sendGood.style.opacity = '0';
        sendGood.style.visibility = 'hidden';
    }, 5000);
})
