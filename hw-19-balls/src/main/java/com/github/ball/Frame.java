package com.github.ball;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    public Frame() {
        setLayout(null);
        setSize(1000, 1000);
        com.github.ball.Panel panel = new Panel();
        panel.setBounds(10, 10, 740, 740);
        panel.setBorder(BorderFactory.createStrokeBorder(new BasicStroke(5.0f)));
        add(panel);
        setVisible(Boolean.TRUE);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
}
