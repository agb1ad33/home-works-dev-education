package com.github.jwt.token;

import java.util.Set;

public class JWTenum {
    private final String principal;
    private final Set<String> authorities;

    public JWTenum(String principal, Set<String> authorities) {
        this.principal = principal;
        this.authorities = authorities;
    }

    public String getPrincipal() {
        return principal;
    }

    public Set<String> getAuthorities() {
        return authorities;
    }
}
