package com.github.http_nio;

public class HttpHandler {

    private final HttpRequest httpRequest;

    public HttpHandler(HttpRequest httpRequest) {
        this.httpRequest = httpRequest;
    }

    public String getResponce() {
        return "HTTP/1.1 200 OK\n\r" +
                String.format("Content-Length: %d\n\r", getHtmlPage().length()) +
                "Content-Type: text/html\n\r\n";
    }

    public String getHtmlPage() {

        return "<!DOCTYPE HTML PUBLIC \"-//IETF//DTD HTML 2.0//EN\">\n\r" +
                "<html>\n\r" +
                "<head>" +
                "<title> Server response </title>" +
                "</head>\n" +
                "<body>\n\r" +
                "<h1>" +
                "Hello, Client! <br>" +
                "Your request type is - " + this.httpRequest.getType() + "<br>\n\r" +
                "Your request body is - " + this.httpRequest.getBody() + "<br>\n\r" +
                "</h1>\n\r" +
                "</body>\n\r" +
                "</html>\n\r\n\r";
    }
}
